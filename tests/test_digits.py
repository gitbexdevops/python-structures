from mock import patch
import pytest

import digits


@pytest.mark.parametrize("num, expected", [
    ("0", 0),
    ("1", 1),
    ("2", 2),
    ("10", 1),
    ("321", 6),
    ("1234567890", 45),
])
@patch('builtins.input')
def test_main(input_mock, num, expected):
    input_mock.return_value = num
    with patch('builtins.print') as print_mock:
        digits.main()
        print_mock.assert_called_with(expected)
