from mock import patch
import pytest

import loop


@patch('builtins.input')
@pytest.mark.parametrize("num, expected", [
    ("0", 1),
    ("1", 1),
    ("2", 2),
    ("3", 6),
    ("5", 120),
])
def test_main(input_mock, num, expected):
    input_mock.return_value = num
    with patch('builtins.print') as print_mock:
        loop.main()
        print_mock.assert_called_with(expected)
