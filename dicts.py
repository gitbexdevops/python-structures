"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())
    dict1 = {key: value for (key, value) in d.items() if value is not None}
    print(dict1)


if __name__ == "__main__":
    main()
