"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = input()
    if int(n) >= 0:
        result = 0
        for i in str(n):
            result += int(i)
    print(result)


if __name__ == "__main__":
    main()
