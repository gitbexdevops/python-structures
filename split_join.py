def main():
    word = input()
    words = word.split(' ')
    reverse_sentence = ""
    for i in words:
        reverse_sentence += i[::-1] + ' '
    print(reverse_sentence.strip(' '))


if __name__ == "__main__":
    main()
#num = "Let's take LeetCode contest", expected = "s'teL ekat edoCteeL tsetnoc"
